//
//  TableViewCell.swift
//  RICK-AND-MORTY-IOS
//
//  Created by Prior Solution on 2/12/2564 BE.
//

import UIKit

class TableViewCellAdapter: UITableViewCell {
    
    @IBOutlet weak var ivPic: UIImageView!

    @IBOutlet weak var tvName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func onHandleCell(results: Results){
        guard let imageUrlString = URL(string: results.image) else { return;}
        ivPic.loadImge(withUrl: imageUrlString)
        tvName.text = results.name
    }
}

extension UIImageView {
    func loadImageUrl(withUrl url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let imageData = try? Data(contentsOf: url) {
                if let image = UIImage(data: imageData) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
