import Foundation

struct HomeViewModel{
        
    func onGetCharacter(onResponse: @escaping ([Results])->()) {
        let service = URL(string: "https://rickandmortyapi.com/api/character")
        
        URLSession.shared.dataTask(with: service!) {
            data, response, error in
            guard let data = data, error == nil else {
                print("something went wrong")
                return
            }
            do {
                let dataResult = try JSONDecoder().decode(CharacterResponse.self, from: data)
                onResponse(dataResult.results)
                print(dataResult)
            } catch {
                
            }
        }.resume()
    }
}
